## Introduction

What does API Resources mean?

**API Resources** acts as a transformation layer that sits between our Eloquent models and the JSON responses that are actually returned by our API.

**API resources** present a way to easily transform our models into `JSON responses`.

## Tutorial

From [here](https://dev.to/dalelantowork/laravel-8-api-resources-for-beginners-2cpa).

## Stack

Laravel Framework 10.x-dev

PHP 8.2.0

## License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
